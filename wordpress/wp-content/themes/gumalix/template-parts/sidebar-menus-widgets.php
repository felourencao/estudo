<?php
/**
 * Displays the menus and widgets at the end of the main element.
 * Visually, this output is presented as part of the sidebar element.
 *
 * @package WordPress
 * @subpackage gumalix
 * @since gumalix 1.0
 */

$has_sidebar_menu = has_nav_menu( 'sidebar' );
$has_social_menu = has_nav_menu( 'social' );
$has_sidebar_1 = is_active_sidebar( 'sidebar-1' );
$has_sidebar_2 = is_active_sidebar( 'sidebar-2' );

// Only output the container if there are elements to display.
if ( $has_sidebar_menu || $has_social_menu || $has_sidebar_1 || $has_sidebar_2 ) {
	?>

	<div id="sidebar-widget"class="sidebar-nav-widgets-wrapper header-sidebar-group l-sidebar">

		<div class="sidebar-inner section-inner">

			<?php

			$sidebar_top_classes = '';

			$sidebar_top_classes .= $has_sidebar_menu ? ' has-sidebar-menu' : '';
			$sidebar_top_classes .= $has_social_menu ? ' has-social-menu' : '';

			if ( $has_sidebar_menu || $has_social_menu ) {
				?>
				<div class="sidebar-top<?php echo $sidebar_top_classes; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?>">
					<?php if ( $has_sidebar_menu ) { ?>

						<nav aria-label="<?php esc_attr_e( 'sidebar', 'gumalix' ); ?>" role="navigation" class="sidebar-menu-wrapper">

							<ul class="sidebar-menu">
								<?php
								wp_nav_menu(
									array(
										'container'      => '',
										'depth'          => 1,
										'items_wrap'     => '%3$s',
										'theme_location' => 'sidebar',
									)
								);
								?>
							</ul>

						</nav><!-- .site-nav -->

					<?php } ?>
					<?php if ( $has_social_menu ) { ?>

						<nav aria-label="<?php esc_attr_e( 'Social links', 'gumalix' ); ?>" class="sidebar-social-wrapper">

							<ul class="social-menu sidebar-social social-icons">

								<?php
								wp_nav_menu(
									array(
										'theme_location'  => 'social',
										'container'       => '',
										'container_class' => '',
										'items_wrap'      => '%3$s',
										'menu_id'         => '',
										'menu_class'      => '',
										'depth'           => 1,
										'link_before'     => '<span class="screen-reader-text">',
										'link_after'      => '</span>',
										'fallback_cb'     => '',
									)
								);
								?>

							</ul><!-- .sidebar-social -->

						</nav><!-- .sidebar-social-wrapper -->

					<?php } ?>
				</div><!-- .sidebar-top -->

			<?php } ?>

			<?php if ( $has_sidebar_1 || $has_sidebar_2 ) { ?>

				<aside class="sidebar-widgets-outer-wrapper" role="complementary">

					<div class="sidebar-widgets-wrapper">

						<?php if ( $has_sidebar_1 ) { ?>

							<div class="sidebar-widgets column-one grid-item">
								<?php dynamic_sidebar( 'sidebar-1' ); ?>
							</div>

						<?php } ?>

						<?php if ( $has_sidebar_2 ) { ?>

							<div class="sidebar-widgets column-two grid-item">
								<?php dynamic_sidebar( 'sidebar-2' ); ?>
							</div>

						<?php } ?>

					</div><!-- .sidebar-widgets-wrapper -->

				</aside><!-- .sidebar-widgets-outer-wrapper -->

			<?php } ?>

		</div><!-- .sidebar-inner -->

	</div><!-- .sidebar-nav-widgets-wrapper -->

<?php } ?>
