<?php
/**
 * Customizer Separator Control settings for this theme.
 *
 * @package WordPress
 * @subpackage gumalix
 * @since gumalix 1.0
 */

if ( class_exists( 'WP_Customize_Control' ) ) {

	if ( ! class_exists( 'gumalix_Separator_Control' ) ) {
		/**
		 * Separator Control.
		 *
		 * @since gumalix 1.0
		 */
		class gumalix_Separator_Control extends WP_Customize_Control {
			/**
			 * Render the hr.
			 *
			 * @since gumalix 1.0
			 */
			public function render_content() {
				echo '<hr/>';
			}

		}
	}
}
