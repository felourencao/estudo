<?php

/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage gumalix
 * @since gumalix 1.0
 */

?>
<footer id="footer" class="l-footer container-fluid" role="contentinfo">
	<div id="copyright">
		&copy; <?php echo esc_html(date_i18n(__('Y', 'blankslate'))); ?> <?php echo esc_html(get_bloginfo('name')); ?>
	</div>	
</footer>

</body>

</html>