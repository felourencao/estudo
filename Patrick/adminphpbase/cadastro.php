<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">
    <script src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="assets/js/script.js"></script>
    
    <title>Document</title>

    <!-- puxar arquivo que faz a conexão com o banco de dados -->
    <?php
    include('assets/php/connect.php');
    ?>

</head>
<main class="bloco">
<form action="assets/php/user.php" method="post">
    <div class="container">
        <h1>Registro</h1>
        <p>Por favor, preenchar todos os dados do formulario.</p>
        <hr>
        <label for="name"><b>Nome</b></label>
        <input type="text" placeholder="Name" name="name" id="name">

        <label for="last-name"><b>Sobrenome</b></label>
        <input type="text" placeholder="Last Name" name="last_name" id="last_name">

        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Email" name="email" id="email">

        <label for="psw"><b>Senha</b></label>
        <input type="password" placeholder="Password" name="password" id="password">

        
        <p>após o cadastro de um perfil admin, recomendamos a desativação dessa página</p>
        <button type="submit" id="cadastrar" name="cadastrar" class="registerbtn">Register</button>
    </div>      

    <div class="container signin">
        <p>Já tem uma conta ? Faça o <a href="index.php">login </a>.</p>
    </div>
</form>
</main>