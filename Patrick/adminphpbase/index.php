<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="assets/js/script.js"></script>

    <title>Document</title>

    <!-- puxar arquivo que faz a conexão com o banco de dados -->
    <?php
    include('assets/php/connect.php');
    ?>

</head>

<body>

    <div class="container-login">
        <div class="card">
            <div class="container-card">
                <form action="/assets/php/validacao.php" method="post">
                    <label for="email"><b>Email</b></label>
                    <input type="text" placeholder="Email" name="email" required>

                    <label for="psw"><b>Senha</b></label>
                    <input type="password" placeholder="Password" name="password" id="psw" required>
                    <button type="submit" class="registerbtn" name="submit">LOGIN</button>
                </form>

                <div>Não tem uma conta ? <a href="cadastro.php">Cadastre</a></div>
            </div>
        </div>
    </div>




</body>

</html>