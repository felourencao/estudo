<?php

require_once('config.php');
require_once('core/controller.Class.php');

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login com o Google</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    <div class="container" style="margin-top: 100px;">
    <?php
        if(isset($_COOKIE["id"]) && isset($_COOKIE["sess"])){
            $Controller = new Controller;
            if($Controller -> checkUserStatus($_COOKIE["id"], $_COOKIE["sess"])){
                echo $Controller -> printData(intval($_COOKIE["id"]));
                echo '<a href="logout.php" type="button" class="btn btn-danger">Logout</a>';
            }else{
                echo "Error!";
            }
        }else{
    ?>
    <img src="../google_login_php/images/logo.jpeg" alt="Logo" style="display: table;margin: 0 auto; max-width: 150px;">
    <form action='' method="POST">
        <!--<div class="form-group">
            <label for="inputEmail1"> Email Address</label>
            <input type="email" class="form-control" id="inputEmail1" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="inputPassword1"> Password</label>
            <input type="password" class="form-control" id="inputPassword1" placeholder="Enter password">
        </div> -->

        <button onclick="window.location = '<?php echo $login_url; ?>' " type="button" class="btn btn-danger" style="display: table;margin: 0 auto;">Login With Google</button>


    </form>
    <?php } ?>
    </div>
</body>
</html>