-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 01-Jul-2021 às 19:42
-- Versão do servidor: 5.7.31
-- versão do PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `google_login`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `session` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `first_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `avatar`, `email`, `password`, `session`, `last_login`, `first_login`) VALUES
(1, 'Patrick', 'Demeti', 'https://lh3.googleusercontent.com/a-/AOh14GiEOCH5FeZlhRGv_Gao17UWJ3Lwji__yBCZ4mjOgA=s96-c', 'patrickdlemos@gmail.com', '7zz9A', '2021-06-22 20:57:47', '2021-06-23 10:00:00', '2021-06-28 16:09:00'),
(2, 'Patrick', 'Demeti', 'https://lh3.googleusercontent.com/a/AATXAJyfiD5G1HbTlwKUEFJmGIhScX9XK0bvp3YFCETo=s96-c', 'patrick.demeti@gumalix.com.br', 'C080x', '2021-06-23 13:11:26', '2021-06-23 13:11:26', '2021-06-28 16:09:00'),
(3, 'Vinicius', 'Fernandes de Araujo', 'https://lh3.googleusercontent.com/a/AATXAJwRxDYv5XzwDahyichBh-1BV6HU9QRoHCHnLfWX=s96-c', 'vinicius.araujo@gumalix.com.br', '7777C', '2021-06-23 13:12:03', '2021-06-23 13:12:03', '2021-06-28 16:09:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
