<?php
require_once 'core/controller.Class.php';
require_once 'config.php';


// Conseguir acesso as informações pessoais do usuario
if(isset($_GET["code"])){
    $token = $gClient->fetchAccessTokenWithAuthCode($_GET["code"]);
}else{
    header('Location: index.php');
    exit();
}

// Erro no token
if(isset($token["error"]) != "invalid_grant"){

$oAuth = new Google_Service_Oauth2($gClient);
$userData = $oAuth->userinfo_v2_me->get();

    /// Colocar data 

    $Controller = new Controller;
    echo $Controller -> insertData(array(
       'email' => $userData['email'],
       'avatar' => $userData['picture'],
        'family' => $userData['familyName'],
        'givenName' => $userData['givenName']
     ));

    //var_dump($userData);


}else{
    header('location: index.php');
    exit();
}



?>
