<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

<div class="container card p-5">
<div class="row">
<div class="offset-1 col-10" >
<form method="post" action="{{ route('ficha', ['idioma'=>'PT', 'produto'=>281]) }}" accept-charset="UTF-8" {{ csrf_token() }}>
<input name="idioma" type="hidden" value="PT"/>
<input name="produto" type="hidden" value="281"/>
<h1>Produtos / imagem / idioma / produto</h1>
<button class="btn btn-primary"> Submit</button>
</form>
</div>
</div>
</div>

<!-- divisão -->

<div class="container card p-5">
<div class="row">
<div class="offset-1 col-10" >
<form method="post" action="{{ route('ficha_ft', ['idioma'=>'PT', 'cod_item'=>00072]) }}" accept-charset="UTF-8" {{ csrf_token() }}>
<input name="idioma" type="hidden" value="PT"/>
<input name="codigo" type="hidden" value="00072"/>
<h1>Produtos / imagem / idioma / Codigo Item</h1>
<button class="btn btn-primary"> Submit</button>
</form>
</div>
</div>
</div>

<!-- divisão -->

<div class="container card p-5">
<div class="row">
<div class="offset-1 col-10" >
<form method="post" action="{{ route('categoria_ft', ['cod_categoria'=>'1']) }}" accept-charset="UTF-8" {{ csrf_token() }}>
<input name="cod-categoria" type="hidden" value="1"/>
<h1>Produtos / imagem / idioma / Codigo Categoria</h1>
<button class="btn btn-primary"> Submit</button>
</form>
</div>
</div>
</div>


<!-- divisão -->

<div class="container card p-5">
<div class="row">
<div class="offset-1 col-10" >
<form method="post" action="{{ route('receitas_ft', ['num_receita'=>'21']) }}" accept-charset="UTF-8" {{ csrf_token() }}>
<input name="num_receita" type="hidden" value="21"/>
<h1>Produtos / imagem / idioma / Numero Receita</h1>
<button class="btn btn-primary"> Submit</button>
</form>
</div>
</div>
</div>

<!-- divisão -->

<div class="container card p-5">
<div class="row">
<div class="offset-1 col-10" >
<form method="post" action="{{ route('receitas_pdt', ['produto'=>'281']) }}" accept-charset="UTF-8" {{ csrf_token() }}>
<input name="prd_receita" type="hidden" value="281"/>
<h1>Produtos / imagem / idioma / Produto Receita</h1>
<button class="btn btn-primary"> Submit</button>
</form>
</div>
</div>
</div>



</body>
</html>