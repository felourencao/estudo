<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    public function login()
    {
    	$client = new Client();
    	$response = $client->request('POST','https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
        ]]);
    	$statusCode = $response->getStatusCode();
    	$body = $response->getBody()->getContents();

    	return $body;


    }

    public function produtos()
    {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];


        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Produtos', [
            'headers' => $headers
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        
        $events = json_decode($body, true);

        return $events;


    }


    public function categorias()
    {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];


        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Produtos/Categorias', [
            'headers' => $headers
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        
        $events = json_decode($body, true);

        return $events;


    }

    public function idiomas()
    {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];


        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Produtos/Idiomas', [
            'headers' => $headers
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        
        $events = json_decode($body, true);

        return $events;


    }


    public function ficha(request $request)
    {       
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];


        $idioma = $request->get('idioma');
        $produto = $request->get('produto');

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Produtos/Imagem/'.$idioma.'/'.$produto, [
            
            'headers' => $headers,
            'content-type' => 'image/jpeg',
            'stream' => true
        ]);

        
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $base64 = base64_encode($body);
        $mime = "image/jpeg";
        $img = ('data:' . $mime . ';base64,' . $base64);
        return "<img src=$img alt='ok'>";
    
        
    } 


    public function ficha_ft(request $request)
    {       
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];


        $idioma = $request->get('idioma');
        $codigo = $request->get('codigo');

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Produtos/Similar/Imagem/'.$idioma.'/'.$codigo, [
            
            'headers' => $headers,
            'content-type' => 'image/jpeg',
            'stream' => true
        ]);

        
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $base64 = base64_encode($body);
        $mime = "image/jpeg";
        $img = ('data:' . $mime . ';base64,' . $base64);
        return "<img src=$img alt='ok'>";
    
        
    } 

         public function categoria_ft(request $request)
    {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];


        $codcat = $request->get('cod-categoria');

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Produtos/Categoria/Imagem/'.$codcat, [
            
            'headers' => $headers,
            'content-type' => 'image/jpeg',
            'stream' => true
        ]);

        
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $base64 = base64_encode($body);
        $mime = "image/jpeg";
        $img = ('data:' . $mime . ';base64,' . $base64);
        return "<img src=$img alt='ok'>";
    
        
    } 

    public function receitas()
    {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];


        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Receitas', [
            'headers' => $headers
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        
        $events = json_decode($body, true);

        return $events;


    }

    public function tipos()
    {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];


        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Receitas/Tipos', [
            'headers' => $headers
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        
        $events = json_decode($body, true);

        return $events;


    }

         public function receitas_ft( Request $request)
    {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];


        $numrec = $request->get('num_receita');

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Receitas/Imagem/'.$numrec, [
            
            'headers' => $headers,
            'content-type' => 'image/jpeg',
            'stream' => true
        ]);

        
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $base64 = base64_encode($body);
        $mime = "image/jpeg";
        $img = ('data:' . $mime . ';base64,' . $base64);
        return "<img src=$img alt='ok'>";
    } 

       public function receitas_pdt(Request $request)
       {
        $client = new Client();
        $response = $client->request('POST', 'https://portal.tirolez.com.br/marketing.api/Usuarios/login', [
            'json' => [
                'CNPJ' =>  '30.119.723/0001-75',
                'SENHA' => 'py#30@119@723',
            ]
        ]);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();


        $events = json_decode($body, true);

        $token = $events['Token'];

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];


        $numpdt = $request->get('prd_receita');

        $response = $client->request('GET', 'https://portal.tirolez.com.br/marketing.api/Receitas/Produto/Imagem/'.$numpdt, [
            
            'headers' => $headers,
            'content-type' => 'image/jpeg',
            'stream' => true
        ]);

        
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $base64 = base64_encode($body);
        $mime = "image/jpeg";
        $img = ('data:' . $mime . ';base64,' . $base64);
        return "<img src=$img alt='ok'>";
    } 
}
