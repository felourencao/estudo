<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/login', [ApiController::class, 'login']);



Route::get('/produtos', [ApiController::class, 'produtos']);
Route::get('/produtos/categorias', [ApiController::class, 'categorias']);
Route::get('/produtos/idiomas', [ApiController::class, 'idiomas']);
Route::post('/produtos/imagem/{idioma}/{produto}', [ApiController::class, 'ficha'])->name('ficha'); /* {produto}=COD_FICHA */
Route::post('/produtos/similar/imagem/{idioma}/{cod_item}', [ApiController::class, 'ficha_ft'])->name('ficha_ft');
Route::post('/produtos/categorias/imagem/{cod_categoria}', [ApiController::class, 'categoria_ft'])->name('categoria_ft');

Route::get('/receitas', [ApiController::class, 'receitas']);
Route::get('/receitas/tipos', [ApiController::class, 'tipos']);
Route::post('/receitas/imagem/{num_receita}', [ApiController::class, 'receitas_ft'])->name('receitas_ft');
Route::post('/receitas/produto/imagem/{produto}', [ApiController::class, 'receitas_pdt'])->name('receitas_pdt');

