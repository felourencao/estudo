<?php

return [
    'message' => 'This site uses cookies',
    'message2' => 'To optimize your browsing experience, we use cookies.<br>By continuing on the site, we consider that you are in agreement with our <a class="linkcookie" href="#">Cookies Policy.</a> ',
    'agree' => 'Agree and Continue', 
];
