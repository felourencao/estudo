<?php

return [
    'message' => 'Este sitio utiliza cookies',
    'message2' => 'Para optimizar su experiencia de navegación, utilizamos cookies. <br>Al continuar en el sitio, consideramos que está de acuerdo con nuestra <a class="linkcookie" href="#">Política de Cookies.</a> ',
    'agree' => 'Aceptar y Continuar', 
];
