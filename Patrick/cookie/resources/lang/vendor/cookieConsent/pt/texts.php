<?php

return [
    'message' => 'Esse site utiliza cookies',
    'message2' => 'Para otimizar sua experiência durante a navegação, fazemos uso de cookies. <br>Ao continuar no site, consideramos que você está de acordo com a nossa <a class="linkcookie" href="#">Política de Cookies.</a> ',
    'agree' => 'Concordo e Continuar', 
];
