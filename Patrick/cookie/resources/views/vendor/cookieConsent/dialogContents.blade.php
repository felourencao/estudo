<div class="js-cookie-consent cookie-consent">

    <p class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </p>
    <p class="cookie-consent__message2">
        {!! trans('cookieConsent::texts.message2') !!}
    </p>

</div>

<div class="js-cookie-consent cookie-btn">

    <button class="js-cookie-consent-agree cookie-consent__agree">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>




