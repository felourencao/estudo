<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\ConfigController;
use App\Http\Controllers\HomeController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Entendendo o Request (1/2)
|--------------------------------------------------------------------------
| 
*/

Route::get('/', HomeController::class);
Route::view('/teste', 'teste');

Route::prefix('/config')->group(function(){

    Route::get('/', [ConfigController::class, 'index']);
    Route::get('info', [ConfigController::class, 'info']);
    Route::get('permissoes', [ConfigController::class, 'permissoes']);

});



/* 
|-----------------------------------------------------------------------------
| Fallback de Rotas (404)
|-----------------------------------------------------------------------------
| */

Route::fallback(function(){
    return view('404');

});

