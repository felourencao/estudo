<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Cadastro</title>
</head>
<?php
include ('assets/php/connection.php'); 
?>
<main class="bloco">
<form action="assets/php/cadastro.php" method="post">
    <div class="container">
        <h1>Register</h1>
        <p>Please fill in this form to create an account.</p>
        <hr>
        <label for="name"><b>Name</b></label>
        <input type="text" placeholder="Name" name="name" id="name">

        <label for="l-name"><b>Last Name</b></label>
        <input type="text" placeholder="Last Name" name="l_name" id="l_name">

        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Email" name="email" id="email">

        <label for="uname"><b>Username</b></label>
        <input type="text" placeholder="Username" name="uname">

        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Password" name="password" id="password">

        
        <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>
        <button type="submit" id="cadastrar" name="cadastrar" class="registerbtn">Register</button>
    </div>      

    <div class="container signin">
        <p>Already have an account? <a href="sign.php">Sign in</a>.</p>
    </div>
</form>
</main>