<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Login</title>
</head>
<main class="bloco">
<div class="container">
    <form action="assets/php/validacao.php" method="post">
        <label for="uname"><b>Username</b></label>
        <input type="text" placeholder="Username" name="uname" required>

        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Password" name="password" id="psw" required>
        <button type="submit" class="registerbtn" name="submit">LOGIN</button>
    </form>
</div>
</main>