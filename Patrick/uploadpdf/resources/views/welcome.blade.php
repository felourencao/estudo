<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />

    </head>
    <body>

    <div class="container">
    <div class="card bg-light mt-3">
        <div class="card-header">
            Upload PDF
        </div>
        <div class="card-body">
        <form  method="post" action="/upload_pdf" enctype='multipart/form-data'>
        {{ csrf_field() }}
        <input type="file" name="pdf" class="form-control"><br>
        <br>
        <button class="btn btn-success">Upload pdf</button>
        </form>
        </div>
    </div>
</div>
              

    </body>
</html>
