<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function upload_pdf(Request $request)
    {
        $fileName = '';

        $request->hasFile('pdf');
        $file = $request->file('pdf');
        $extension = $file->getClientOriginalExtension();
        $fileName = 'pdf_'.time().'.'.$extension;
        $path = public_path().'/pdf';
        $upload = $file->move($path,$fileName);

        DB::table('scripts')->insert(['file' => $fileName]);



      return redirect('/')->with('upload','realizado com sucesso');

    }
}


