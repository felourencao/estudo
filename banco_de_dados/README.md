## Passo 1

Acessar localhost/phpmyadmin

## Passo 2

utilizador: root
senha: ""
servidor: MySQL

Executar
## Passo 3

Clicar em Novo

Nomear Banco de Dados 

selecionar utf8_general_ci

Criar

## Passo 4

Criar tabela

Definir nome relativo ao projeto (ex: users, partners, clients)

Guarda

## Passo 5 

Definir nome das colunas (A Primeira SEMPRE id, tipo INT com A_I tickado)

as próximas relativas ao que precisa (ex: nome, data de nascimento, altura, email)

Tipo = VARCHAR = Numero, letras e caracteres especiais
Tamanho/Valores = limite de caracteres (max 255)

Guardar
